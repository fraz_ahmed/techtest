package messageApp;
import java.util.*;

import java.util.List;

public class Product {
	String name;
	
	Product(String str){
		this.name = str;
	}
	public String getName() {
		return this.name;
	}
	public static boolean isProductExists(final ArrayList<Product> list, final String name){
	    return list.stream().filter(o -> o.getName().equals(name)).findFirst().isPresent();
	}
}
