package messageApp;
import java.util.*;


public class Sale {
	Product product;
	int value;
	int numOfOccurrences;
	
	Sale(Product p,int v){
		product = p;
		value= v;
		numOfOccurrences = 1;
	}
	Sale(Product p,int v, int num){
		product = p;
		value= v;
		numOfOccurrences = num;
	}
	public String getProductName() {
		return this.product.name;
	}
	public int getValue() {
		return this.value;
	}
	public int getNumOfOccurrences(){
		return this.numOfOccurrences;
	}
	
	public void adjust(String action, int num)
	{
		if(action.equals("add")) {
			value = value + num;
		} else if(action.equals("subtract")) {
			value = value - num;
		} else	{
			value = value * num;
		}
	}
	public static void perform(ArrayList<Sale> list, String pName, String action, int num){
		list.stream().filter(o -> o.getProductName().equals(pName)).forEach(
            o -> {
               o.adjust(action, num);
            }
	    );
	}
}
