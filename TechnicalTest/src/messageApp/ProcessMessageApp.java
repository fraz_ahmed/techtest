/**
 * 
 */
package messageApp;
import java.util.*;
import java.lang.*;
/**
 * @author Fraz
 *
 */
public class ProcessMessageApp {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Product> productList = new ArrayList<Product>();
		ArrayList<Sale> saleList = new ArrayList<Sale>();
		ArrayList<String> adjustmentsList = new ArrayList<String>();
		int messageCount = 0;
		
		String Message ;
		Scanner sc = new Scanner(System.in);
		
		while(messageCount < 50) {
			
			System.out.println("\nEnter your message.(For exit the application enter 'exit')");
			Message = sc.nextLine();
			
			if(Message.equals("exit") || Message.equals("Exit")) {
				break;
			}
		
			String[] messageParts = Message.split(" ");
			messageCount ++ ;
			
			if(messageParts.length == 3) {
				
				if(messageParts[0].equals("add") || messageParts[0].equals("subtract") || messageParts[0].equals("multiply") ) {
					
					if(Product.isProductExists(productList, messageParts[2])) {
						
						String numberOnly= messageParts[1].replaceAll("[^0-9]", "");
						int amountInt = Integer.parseInt(numberOnly);
						Sale.perform(saleList, messageParts[2], messageParts[0], amountInt);
						String str = "Adjustment " + messageParts[0] + " amount " + numberOnly + "in " + messageParts[2];
						adjustmentsList.add(str);
						System.out.println("Adjustment is done");
					} else {
						System.out.println("No such product exists.");
					}
				
				} else {
				
					if(Product.isProductExists(productList, messageParts[0])) {
						
						Product messageProduct = new Product(messageParts[0]);
						String numberOnly= messageParts[2].replaceAll("[^0-9]", "");
						int amountInt = Integer.parseInt(numberOnly);
						Sale messageSale = new Sale(messageProduct,amountInt);  
						saleList.add(messageSale);
						
						System.out.println("Sale is added (1 occurrence).");
						
					} else {
						
						Product messageProduct = new Product(messageParts[0]);
						productList.add(messageProduct);
	
						String numberOnly= messageParts[2].replaceAll("[^0-9]", "");
						int amountInt = Integer.parseInt(numberOnly);
						Sale messageSale = new Sale(messageProduct,amountInt);  
						saleList.add(messageSale);
						
						System.out.println("Product and sale are added successfully(1 occurrence).");
					}
				
				}
			
			} else {
				
				if(Product.isProductExists(productList, messageParts[3])) {
					
					Product messageProduct = new Product(messageParts[3]);
					String numberOnly= messageParts[5].replaceAll("[^0-9]", "");
					int amountInt = Integer.parseInt(numberOnly);
					int numOfOccInt = Integer.parseInt(messageParts[0]);
					Sale messageSale = new Sale(messageProduct,amountInt,numOfOccInt);  
					saleList.add(messageSale);
					
					System.out.println("Sale is added (multiple occurrences).");
					
				} else {
					
					Product messageProduct = new Product(messageParts[3]);
					productList.add(messageProduct);
					String numberOnly= messageParts[5].replaceAll("[^0-9]", "");
					int amountInt = Integer.parseInt(numberOnly);
					int numOfOccInt = Integer.parseInt(messageParts[0]);
					Sale messageSale = new Sale(messageProduct,amountInt,numOfOccInt);  
					saleList.add(messageSale);
					
					System.out.println("Product and sale are added successfully (multiple occurrences).");
				}
			
			}
			
			if(messageCount % 10 == 0) {
				System.out.println("\nLog after every 10th message (Product Name, Num of Sales, Total Amount)");
				for(Product p:productList) {
					int totalAmount = 0;
					int totalSales = 0;
					for(Sale s:saleList) {
						if(p.getName().equals(s.getProductName())) {
							totalAmount = totalAmount + (s.getNumOfOccurrences() * s.getValue());
							totalSales = totalSales + s.getNumOfOccurrences(); 
						}
					}
					System.out.println(p.getName() + " " + totalSales + " " + totalAmount);
				}
			}
		}
		sc.close();
		
		
		if(messageCount == 50) {
			System.out.println("\nApplication is pausing and here is all the adjustments");
			for(String str:adjustmentsList) {
				System.out.println(str);
			}
		}
		
		System.out.println("\nHere is all the Products");
		for(Product p:productList) {
			System.out.println(p.getName());
		}
		
		System.out.println("\nHere is all the Sales");
		for(Sale s:saleList) {
			System.out.println(s.getProductName() + " " + s.getValue() + " " + s.getNumOfOccurrences() );
		}
		
		
		System.out.println("\nExit Application");
	}

}
